
#!/bin/sh
#---------------------#
# Backups:            #
#   * MongoDB         #
# Date: Junio/2022    #
# Version: 1.0        #
# Autor: J. Andres B. #
#---------------------#

# execute on current path
cd "$(dirname "$0")"

# Variables
DATE="$(date +%Y-%m-%d)"

required=(
  MONGODB_URI
  S3_BUCKET
  AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY
  AWS_DEFAULT_REGION
)

for v in "${required[@]}"; do
  if [[ -z "${!v}" ]]; then
      echo "Required env var $v not set!"
      exit 1
  fi
done


# mongo backup
echo "Start Dumping all MongoDB databases to compressed archive..."

mongodump --archive=mongodb.dump \
	--gzip \
	--uri "$MONGODB_URI"


echo "Zip MongoDB backup..."
gzip mongodb.dump

# Copy backups to S3
echo "Uploading compressed archive to S3 bucket..."

aws s3 cp mongodb.dump.gz "$S3_BUCKET/$DATE.dump.gz"

echo "Cleaning up compressed archive..."

# delete mongo backup folde
rm -rf mongodb.dump.gz || true

echo "Backup Complete..."
