FROM alpine:latest

LABEL   Version="1.0" \
        Autor="J. Andres B. G." \
        Date="Junio 2022"

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/main' >> /etc/apk/repositories && \
    echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/community' >> /etc/apk/repositories      

RUN apk update && \
    apk add mongodb-tools && \
    apk add --no-cache aws-cli \
    && rm -rf /var/cache/apk/*

WORKDIR /scripts

COPY backup.sh .

RUN chmod +x backup.sh

CMD /scripts/backup.sh
