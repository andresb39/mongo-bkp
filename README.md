# k8s cronjob for MongoDB backup
Kubernetes CronJob to perform Mongo database backups. 

## Requirements

We need a kubernetes secret with the following values 
- MONGODB_URI
- S3_BUCKET
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION
## TODO
- [X] Backup database.
- [X] Zip backup database.
- [X] Upload backup to AWS S3
